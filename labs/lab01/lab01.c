#include "pico/stdlib.h"
void whileloop(uint pin, uint del);

int main() {

    // Specify the PIN number and sleep delay
    const uint LED_PIN   =  25;
    const uint LED_DELAY = 500;

    // Setup the LED pin as an output.
    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);

    // Do forever...
    whileloop(LED_PIN, LED_DELAY);

    // Should never get here due to infinite while-loop.
    return 0;

}

void whileloop(uint pin, uint del){
    while (true) {

        // Toggle the LED on and then sleep for delay period
        gpio_put(pin, 1);
        sleep_ms(del);

        // Toggle the LED off and then sleep for delay period
        gpio_put(pin, 0);
        sleep_ms(del);

    }
}
