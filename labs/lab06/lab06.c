#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pico/stdlib.h"
#include "pico/float.h"     // Required for using single-precision variables.
#include "pico/double.h"    // Required for using double-precision variables.
#include "pico/multicore.h" // Required for using multiple cores on the RP2040.


/**
 * @brief This function acts as the main entry-point for core #1.
 *        A function pointer is passed in via the FIFO with one
 *        incoming int32_t used as a parameter. The function will
 *        provide an int32_t return value by pushing it back on 
 *        the FIFO, which also indicates that the result is ready.
 */
void core1_entry() {
    while (1) {
        // 
        int32_t (*func)() = (int32_t(*)()) multicore_fifo_pop_blocking();
        int32_t p = multicore_fifo_pop_blocking();
        int32_t result = (*func)(p);
        multicore_fifo_push_blocking(result);
    }
}

// Main code entry point for core0.

float Wallis1Calc(int loops){
    uint64_t prev = time_us_64();
    float wallis = 1;
    float one = 1;
    float two = 2;
    for(int i=1; i <= loops; i++){
        float frac1 = (two * i)/((two * i)-one);
        float frac2 = (two * i)/((two * i)+one);
        wallis = wallis * (frac1 * frac2);
    }
    uint64_t aft = time_us_64();                                        
    uint64_t dif = aft - prev;
    printf("Single-precision execution time: %.1f\n", (double)dif);
    return printf("Single-precision Wallis product: %.3f\n", wallis * 2);
}

double Wallis2Calc(int loops){
    uint64_t prev = time_us_64();
    double wallis = 1;
    double one = 1;
    double two = 2;
    for(int i=1; i <= loops; i++){
        double frac1 = (two * i)/((two * i)-one);
        double frac2 = (two * i)/((two * i)+one);
        wallis = wallis * (frac1 * frac2);
    }
    uint64_t aft = time_us_64();                                        
    uint64_t dif = aft - prev;
    printf("Double-precision execution time: %.1f\n", (double)dif);
    return printf("Double-precision Wallis product: %.3f\n", wallis * 2); 
}
int main() {

    const int    ITER_MAX   = 100000;

    stdio_init_all();
    multicore_launch_core1(core1_entry);

    // Code for sequential run goes here…
    uint64_t prev = time_us_64();                                       //    Take snapshot of timer and store
    Wallis1Calc(ITER_MAX);                                              //    Run the single-precision Wallis approximation
    Wallis2Calc(ITER_MAX);                                              //    Run the double-precision Wallis approximation
    uint64_t aft = time_us_64();                                        //    Take snapshot of timer and store
    uint64_t dif = aft - prev;                     
    printf("Sequential time is %.1f \n", (double)dif);                 //    Display time taken for application to run in sequential mode

    // Code for parallel run goes here…    
    uint64_t prev2 = time_us_64();                                      //    Take snapshot of timer and store     
    multicore_fifo_push_blocking((uintptr_t) Wallis2Calc);              //    Run the single-precision Wallis approximation on one core
    multicore_fifo_push_blocking(ITER_MAX);            
    Wallis1Calc(ITER_MAX);                                              //    Run the double-precision Wallis approximation
    multicore_fifo_pop_blocking();
    uint64_t aft2 = time_us_64();
    uint64_t dif2 = aft2 - prev2;                                       //    Take snapshot of timer and store
    printf("Parallel time is %.1f\n", (double)dif2);                   //    Display time taken for application to run in parallel mode
   

    return 0;
}

