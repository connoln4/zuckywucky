//#define WOKWI             // Uncomment if running on Wokwi RP2040 emulator.

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
// #include "pico/stdlib.h"
#include "pico/float.h"
#include "pico/double.h"

/**
 * @brief EXAMPLE - HELLO_C
 *        Simple example to initialise the IOs and then 
 *        print a "Hello World!" message to the console.
 * 
 * @return int  Application return code (zero for success).
 */

float Wallis1Calc(float wallis);
double Wallis2Calc(double wallis);
int main() {

#ifndef WOKWI
    // Initialise the IO as we will be using the UART
    // Only required for hardware and not needed for Wokwi
    stdio_init_all();
#endif

    float piRef1 = 3.14159265359;
    float wallis1 = 1;
    wallis1 = Wallis1Calc(wallis1);
    float approxError1 = piRef1 - (wallis1 * 2);
    printf ("Single-precision Wallis product %.11f\n", wallis1 * 2);
    printf("Single-precision approximation error is %.11f\n", approxError1);

    double piRef2 = 3.14159265359;
    double wallis2 = 1;
    wallis2 = Wallis2Calc(wallis2);
    double approxError2 = piRef2 - (wallis2 * 2);
    printf ("Double-precision Wallis product %.11f\n", wallis2 * 2);
    printf("Double-precision approximation error is %.11f\n", approxError2);
    // Returning zero indicates everything went okay.
    return 0;
}

float Wallis1Calc(float wallis){
    float one = 1;
    float two = 2;
    for(int i=1; i <= 100000; i++){
        float frac1 = (two * i)/((two * i)-one);
        float frac2 = (two * i)/((two * i)+one);
        wallis = wallis * (frac1 * frac2);
    }
    return wallis;
    
}

double Wallis2Calc(double wallis){
    double one = 1;
    double two = 2;
    for(int i=1; i <= 100000; i++){
        double frac1 = (two * i)/((two * i)-one);
        double frac2 = (two * i)/((two * i)+one);
        wallis = wallis * (frac1 * frac2);
    }
    return wallis;
}